package thread;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

import main.Main;
import network.Network;

import constant.SystemConstants;

public class ReceiveMessageThread extends Thread{
	
	public static int option = 0;
	private static boolean closed = false;
	private static DataInputStream is = null;
	private static PrintStream os = null;
	private int portNumber = SystemConstants.DEFAULT_PORTNUMBER;
	
	public ReceiveMessageThread(DataInputStream is, PrintStream os){
		this.is= is;
		this.os = os;
	}
	/*
	   * Create a thread to read from the server. (non-Javadoc)
	   * 
	   * @see java.lang.Runnable#run()
	   */
	  public void run() {
	    /*
	     * Keep on reading from the socket till we receive "Bye" from the
	     * server. Once we received that then we want to break.
	     */
	    String responseLine;
	    try {
	      while ((responseLine = is.readLine()) != null) {
	        if (responseLine.equals(SystemConstants.FLAG_MESSAGE)){
	        	Main.finish = true;
	        }else if(responseLine.equals(SystemConstants.RESULT_VIEW)){
	        	option = 0;
	        	System.out.println("Hien tren Server khong co file chia se nao.");
	        	Main.finish = true;
	        }else if(responseLine.equals(SystemConstants.RESULT_SEARCH)){
	        	option = 0;
	        	System.out.println("Khong tim thay file.");
	        	Main.finish = true;
	        }else if(responseLine.startsWith("@")){
	        	String info = "";
	        	info = responseLine + " " + Network.getIPAddress() + " " + portNumber;
	        	new SendFileThread(portNumber).start(); // create send file thread to send file to client need download file
	        	portNumber++;
	        	os.println(info); // send IP and PORT to server
	        }else{
	        	if(option == SystemConstants.OPTION_1){
	        		System.out.println("Danh sach cac file hien co tren Server");
	        		option = 0;
	        	}
	        	if(option == SystemConstants.OPTION_2){
	        		System.out.println("Danh sach ten cac tap tin tim duoc:");
	        		option = 0;
	        	}
	        	if(option == SystemConstants.OPTION_3){
	        		option = 0;
	        		String[] info = responseLine.split("\\s"); // receive info of client have file need download
	        		int portNumber = Integer.parseInt(info[1]);
	        		new DownloadFileThread(info[0], portNumber).start(); // download file from other client
	        	}else{
	        		System.out.println("   " + responseLine); // show list of file name
	        	}
	        }
	        if(responseLine.equals(SystemConstants.QUIT)){
	        	Main.finish = true;
	        	break;
	        }
	      }
	      closed = true;
	    } catch (IOException e) {
	      System.err.println("IOException:  " + e);
	    }
	  }
}
