package thread;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import constant.SystemConstants;

public class SendFileThread extends Thread{
	
	// The server socket.
	private static ServerSocket serverSocket = null;
	// The client socket.
	private static Socket clientSocket = null;
	private DataInputStream is = null;
	private PrintStream os = null;
	
	public SendFileThread(int portNumber){
		try{
			serverSocket = new ServerSocket(portNumber);
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	public void run(){
		try{
			clientSocket = serverSocket.accept();
			is = new DataInputStream(clientSocket.getInputStream());
			os = new PrintStream(clientSocket.getOutputStream());
			String receive = "";
			receive = is.readLine();
			String dirDownload = SystemConstants.SHARE_FOLDER + "/" + receive;
			File file = new File(dirDownload);
			os.println("" + file.length());
			byte[] buf = new byte[1024];
        	FileInputStream fis = new FileInputStream(file);
        	BufferedInputStream bis = new BufferedInputStream(fis);
        	int count = 0;
        	while ((count = bis.read(buf)) > 0) {
        		os.write(buf,0,buf.length);
        	}
            bis.close();
            os.flush();
			os.close();
			is.close();
			clientSocket.close();
			serverSocket.close();
		}catch(IOException e){
			System.out.println(e);
		}
	}
}
