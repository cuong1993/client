package thread;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import main.Main;

import constant.SystemConstants;

public class DownloadFileThread extends Thread {
	
	// The client socket
	private static Socket clientSocket = null;
	// The output stream
	private static PrintStream os = null;
	private static DataInputStream is = null;
	public static boolean finish = false;
	
	public DownloadFileThread(String host, int portNumber){
		try{
			System.out.println("   >> Dang ket noi voi client nam giu tap tap tin");
			clientSocket = new Socket();
			clientSocket.connect(new InetSocketAddress(host, portNumber), SystemConstants.TIMEOUT);
			os = new PrintStream(clientSocket.getOutputStream());
			is = new DataInputStream(clientSocket.getInputStream());
			System.out.println("   >> Ket noi thanh cong va dang tai tap tin");
		}catch (IOException e) {
			System.err.println("Khong the ket noi den Server " + host);
		}
	}
	public void run(){
		try {
			os.println(Main.filename);
			byte [] buf = new byte [1024];
			FileOutputStream fos = new FileOutputStream(SystemConstants.DOWNLOAD_FOLDER + "/" + Main.filename);
			BufferedOutputStream bos = new BufferedOutputStream(fos);
			String length = "0";
			length = is.readLine();
			long size = Long.parseLong(length);
			int count = 0;
			while (size > 0) {
				if (size < 1024){
					buf = new byte[(int)size];
				}
				count = is.read(buf);
				fos.write(buf, 0 , count);
				size -= count;
			}
			fos.close();
		    bos.close();
			System.out.println("   >> Tai tap tin " + Main.filename + " thanh cong");
			os.close();
			is.close();
			clientSocket.close();
			Main.finish = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
