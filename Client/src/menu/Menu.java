package menu;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import constant.SystemConstants;

import models.Account;

public class Menu {

	private Scanner scan = new Scanner(System.in);
	
	public void createMenu(){
		System.out.println("######## Vui long chon thao tac ########");
		System.out.println("   1. Dang nhap vao he thong.");
		System.out.println("   2. Dang ky tai khoan.");
	}
	
	public int chooseMenu() {
		int choose = 0;
		do{
			System.out.print("Chon: ");
			choose = Integer.parseInt(scan.nextLine());
		}while(choose < SystemConstants.OPTION_1 || choose > SystemConstants.OPTION_2);
		return choose;
	}
	
	public void signUp(Account account){
		String name = "";
		String pass = "";
		System.out.println("######## Dang ky tai khoan ########");
		do{
			System.out.print("   Username: ");
			name = scan.nextLine();
		}while(!checkName(name));
		do{
			System.out.print("   Password: ");
			pass = scan.nextLine();
		}while(!checkPassword(pass));
		account.setAccountName(name);
		account.setPassword(pass);
		checkPassword(pass);
		
	}
	
	public void signIn(Account account){
		String name = "";
		String pass = "";
		System.out.println("######## Dang nhap vao he thong ########");
		do{
			System.out.print("   Username: ");
			name = scan.nextLine();
		}while(!checkName(name));
		do{
			System.out.print("   Password: ");
			pass = scan.nextLine();
		}while(!checkPassword(pass));;
		account.setAccountName(name);
		account.setPassword(pass);
		checkPassword(pass);
	}
	
	public void createSubMenu() {
		System.out.println("######## Vui long chon thao tac ########");
		System.out.println("   1. Xem tat ca ten tap tin tren Server.");
		System.out.println("   2. Tim kiem mot ten file tren Server.");
		System.out.println("   3. Download mot file tren Server.");
		System.out.println("   4. Dong ket noi voi Server");
	}
	
	public int chooseSubMenu(int choose) {
		do{
			System.out.print("Chon: ");
			choose = Integer.parseInt(scan.nextLine());
		}while(choose < SystemConstants.OPTION_1 || choose > SystemConstants.OPTION_4);
		return choose;
	}
	
	public boolean checkName(String name) {
		if(name.equals("")){
			System.out.println("Tai khoan khong hop le. Vui long nhap tai khoan.");
			return false;
		}
		Pattern pattern = Pattern.compile(SystemConstants.PATTERN_ACCOUNT);
		Matcher matcher = pattern.matcher(name);
		
		boolean b = matcher.find();//There is a special character in name
		if (b){
		   System.out.println("Tai khoan khong hop le. Tai khoan khong duoc co ky tu dac biet.");
		   return false;
		}else{
		    return true;
		}
	}
	public boolean checkPassword(String password) {
		if(password.equals("")){
			System.out.println("Mat khau khong hop le. Mat khau phai co toi thieu 6 ky tu va khong co khoang trang.");
			return false;
		}
		String pattern = SystemConstants.PATTERN_PASSWORD;
		boolean b = password.matches(pattern);
		if(b){
			return true;
		}else{
			System.out.println("Mat khau khong hop le. Mat khau phai co toi thieu 6 ky tu va khong co khoang trang.");
			return false;
		}
	}
}
