package constant;

public interface SystemConstants {
	public static final String HOST = "localhost";
	public static final int PORT_NUMBER = 2222;
	
	public static final int TIMEOUT = 3000;
	public static final int DEFAULT_PORTNUMBER = 3000;
	
	public static final int OPTION_1 = 1;
	public static final int OPTION_2 = 2;
	public static final int OPTION_3 = 3;
	public static final int OPTION_4 = 4;
	
	public static final String CONNECT_SUCCESS = "Ket noi Server thanh cong !!!!!";
	public static final String PATTERN_ACCOUNT = "[^A-Za-z0-9]";
	public static final String PATTERN_PASSWORD = "(?=\\S+$).{6,}";
	public static final String SIGN_UP = "sign_up";
	public static final String SIGN_IN = "sign_in";
	
	public static final String SIGNIN_SUCCESS = "=> Dang nhap thanh cong";
    public static final String SIGNIN_FAIL = "=> Dang nhap that bai";
    
    public static final String SIGNUP_SUCCESS = "=> Dang ky thanh cong";
    public static final String SIGNUP_FAIL = "=> Dang ky that bai";
    
    public static final String SHARE_FOLDER = "share";
    public static final String DOWNLOAD_FOLDER = "download";
    public static final String FLAG_MESSAGE = "-1";
    
    public static final String VIEW_FILE = "xem";
    public static final String SEARCH_FILE = "tim";
    public static final String RESULT_VIEW = "0";
    public static final String RESULT_SEARCH = "00";
    
    public static final String DOWNLOAD_FILE = "download";
    public static final String CLOSED_CONNECT = "close";
    public static final String QUIT = ">> Dong ket noi voi Server >>";
}
