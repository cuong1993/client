package main;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Scanner;

import thread.DownloadFileThread;
import thread.ReceiveMessageThread;
import thread.SendFileThread;

import network.Network;

import menu.Menu;
import models.Account;

import constant.SystemConstants;

public class Main {

	// The client socket
	private static Socket clientSocket = null;
	// The output stream
	private static PrintStream os = null;
	private static DataInputStream is = null;
	private static boolean closed = false;
	private static Scanner scan = new Scanner(System.in);
	private static Menu menu = new Menu();
	public static boolean finish = false;
	public static String filename = "";
	
	/*
	 * send list of file name to server
	 */
	public static void sendFileName() {
		File folder = new File(SystemConstants.SHARE_FOLDER);
		String []results = folder.list(); // get list of shared file from folder "share"
		if(null != results){
			if(results.length == 0){
				System.out.println("Khong co tap tin chia se nao");
			}else{
				System.out.println("Gui danh sach ten cac tap tin chia se cho Server");
			}
			for (int i = 0; i < results.length; i++) {
				os.println(results[i]); // send list of shared file
			}
		}
		os.println(SystemConstants.FLAG_MESSAGE); // send end message to server
	}
	/*
	 * mainMenu execute sign in and sign up 
	 */
	public static void mainMenu() {
		try{
			String responseLine = "";
			int choose = 0;
			boolean isSignIn = false;
			while(true){
				if(isSignIn){
					// when sign up successfully, you must sign in
					choose = SystemConstants.OPTION_1;
				}else{
					// create menu sign in and sign up
					menu.createMenu();
					choose = menu.chooseMenu();
				}
				if(choose == SystemConstants.OPTION_1){
					/*
					 *  execute sign in
					 */
					isSignIn = false;
					os.println(SystemConstants.SIGN_IN); // send message to sever to sign in
					Account account = new Account();
					menu.signIn(account);
					os.println(account.getAccountName()); // send account to server
					os.println(account.getPassword()); // send password to server
					responseLine = is.readLine(); // receive message sign in successfully or fail
					System.out.println(responseLine); // show message sign in successfully or fail
					if (responseLine.equals(SystemConstants.SIGNIN_SUCCESS)){
						/*
						 * if sign in successfully, send list of shared file in folder "share" and 
						 * start thread receive message from server
						 */
	    				sendFileName();
	    				
	    				new ReceiveMessageThread(is, os).start(); // start thread receive message from server
	    				
						subMenu();
						break;
					}
				}
				if(choose == SystemConstants.OPTION_2){
					// excute sign up
					os.println(SystemConstants.SIGN_UP); // send message to sever to sign up
					Account account = new Account();
					menu.signUp(account);
					os.println(account.getAccountName()); // send account to server
					os.println(account.getPassword()); // send password to server
					responseLine = is.readLine(); // receive message sign up successfully or fail
					System.out.println(responseLine); // show message sign up successfully or fail
					if(responseLine.equals(SystemConstants.SIGNUP_SUCCESS)){
						isSignIn = true;
					}
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/*
	 * create sub menu to execute receive list of file from server, search file,
	 * download file and close connect to server
	 */
	public static void subMenu() {
		try{
			String responseLine = "";
			int choose = 0;
			while (!closed) {
				/*
				 * Create menu:
				 * 1. receive list of file from server
				 * 2. search file
				 * 3. download file
				 * 4. close connect to server
				 */
				menu.createSubMenu();					
				choose = menu.chooseSubMenu(choose);
				if(choose == SystemConstants.OPTION_1){
					ReceiveMessageThread.option = SystemConstants.OPTION_1;
					os.println(SystemConstants.VIEW_FILE); // send message to server to get all shared file
					
					waitThread(); // wait server response
				}else if(choose == SystemConstants.OPTION_2){
					ReceiveMessageThread.option = SystemConstants.OPTION_2;
					os.println(SystemConstants.SEARCH_FILE); // send message to server to execute search file
					String keyword = "";
					System.out.print("Nhap ten tap tin can tim kiem: ");
					keyword = scan.nextLine();
					os.println(keyword); // send file name need search to server
					
					waitThread(); // wait server response
				}else if(choose == SystemConstants.OPTION_3){
					ReceiveMessageThread.option = SystemConstants.OPTION_3;
					os.println(SystemConstants.DOWNLOAD_FILE); // send message to server to execute download file
					System.out.print("Nhap ten tap tin can tai: ");
					filename = scan.nextLine();
					os.println(filename); // send file name need download to server
					
					waitThread(); // wait server response
				}else if(choose == SystemConstants.OPTION_4){
					os.println(SystemConstants.CLOSED_CONNECT); // send message to server to close connect
					waitThread(); // wait server response
					closed = true;
				}else{
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/*
	 * wait thread finish work
	 */
	public static void waitThread() {
		while(!finish){
			System.out.print("");
		}
		finish = false;	
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String host = SystemConstants.HOST;
		int portNumber = SystemConstants.PORT_NUMBER;
		
		System.out.println("Nhap thong tin ket noi den server:");
		System.out.print("   IP: ");
		host = scan.nextLine();
		System.out.print("   Port: ");
		portNumber = Integer.parseInt(scan.nextLine());
		System.out.println("IP = " + host + " / Port = " + portNumber);
		/*
		 * Open a socket on a given host and port. Open input and output streams.
		 */
		try{
			System.out.println(">>>>> Dang ket noi den server >>>>>");
			clientSocket = new Socket();
			clientSocket.connect(new InetSocketAddress(host, portNumber), SystemConstants.TIMEOUT);
			os = new PrintStream(clientSocket.getOutputStream());
			is = new DataInputStream(clientSocket.getInputStream());
		}catch (IOException e) {
			System.err.println("Khong the ket noi den Server " + host);
		}
		/*
		 * If everything has been initialized then we want to write some data to the
		 * socket we have opened a connection to on the port portNumber.
		 */
		if (clientSocket != null && os != null && is != null) {
			try{
				String responseLine = "";
				responseLine = is.readLine();
				System.out.println(responseLine);
				if(responseLine.equals(SystemConstants.CONNECT_SUCCESS)){
					mainMenu();
				}
				/*
				 * Close the output stream, close the input stream, close the socket.
				 */
				os.close();
				is.close();
				clientSocket.close();
			}catch(IOException e){
				System.err.println("IOException:  " + e);
			}
		}
	}

}
