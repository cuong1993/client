package threads;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import network.TCPNetwork;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import constants.SystemConstants;

import dto.OtherComputersInfo;
import event.EventInfo;
import event.EventAction;

public class OpenFileThread extends Thread{
	private static Logger LOGGER = Logger.getLogger(OpenFileThread.class.getName());
	private String posfile = null;
	private transient Vector listeners;
	
	public OpenFileThread(String posfile) {
		super();
		this.posfile = posfile;
	}
	private void readFile(){
		String log = "";
		try {
			if (listeners != null && !listeners.isEmpty()){
				Vector targets;
                synchronized (this) {
                    targets = (Vector) listeners.clone();
                }
                EventInfo event = new EventInfo();
                
				ArrayList<OtherComputersInfo> aNodesInfo = null;
				aNodesInfo = OtherComputersInfo.getListInstance();
				File fXmlFile = new File(posfile);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
	
				log += "Root element :" + doc.getDocumentElement().getNodeName();
	
				NodeList nList = doc.getElementsByTagName(SystemConstants.TAG_COMPUTER);
				int length = nList.getLength();
				event.setMaxValue(length);
				
				// Start event
				Enumeration e = targets.elements();
                while (e.hasMoreElements()) {
                    EventAction l = (EventAction) e.nextElement();
                    l.onStart(event);
                }
				for (int temp = 0; temp < length; temp++) {
	
					Node nNode = nList.item(temp);
	
					log += "\n----------------------------";
					log += "\nCurrent Element :" + nNode.getNodeName();
	
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	
						Element eElement = (Element) nNode;
						String ip = eElement.getElementsByTagName(SystemConstants.TAG_IP_ADDRESS).item(0).getTextContent();
						int port = Integer.parseInt(eElement.getElementsByTagName(SystemConstants.TAG_PORT_NUMBER).item(0).getTextContent());
						
						boolean bConn = TCPNetwork.Connect(ip, port, SystemConstants.CONNECT_MESSAGE);
						OtherComputersInfo otherComputerInfo = new OtherComputersInfo(ip, port, bConn);
						aNodesInfo.add(otherComputerInfo);
						
						log += "\nIP Address : " + aNodesInfo.get(temp).getIpAddress();
						log += "\nPort Number : " + aNodesInfo.get(temp).getPortNumber();
					}
					event.setValue(temp + 1);
					// Event Occur
					e = targets.elements();
                    while (e.hasMoreElements()) {
                        EventAction l = (EventAction) e.nextElement();
                        l.onOccur(event);
                    }
				}
				//Event Finish
                e = targets.elements();
                while (e.hasMoreElements()) {
                    EventAction l = (EventAction) e.nextElement();
                    l.onFinish(event);
                }
				LOGGER.info(log);
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void run(){
		readFile();
	}
	/**
     * register event
     */
    synchronized public void addCustomListener(EventAction l) {
        if (listeners == null) {
            listeners = new Vector();
        }
        listeners.addElement(l);
    }
    /**
     * delete event
     */
    synchronized public void removeCustomListener(EventAction l) {
        if (listeners == null) {
            listeners = new Vector();
        }
        listeners.removeElement(l);
    }
}
