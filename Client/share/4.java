package network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.logging.Logger;

import constants.SystemConstants;
import dto.ListFileName;


public class TCPNetwork {
	private static Logger LOGGER = Logger.getLogger(TCPNetwork.class.getName());
	/**
	 * Connect to other computer
	 * @param ip // ip address
	 * @param port // port number
	 * @param content // content is send to other computer
	 * @return
	 */
	public static boolean Connect(String ip, int port, String content){
		Socket socket = null;
	  try {
	       socket = new Socket();
	       socket.connect(new InetSocketAddress(ip, port), 3000); // 3000 miliseconds = 3s to check timeout    

           DataOutputStream outToServer = new DataOutputStream(socket.getOutputStream());
           // send request
           outToServer.writeBytes(content + '\n');
           
           BufferedReader inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
           
           if(content.equals(SystemConstants.CONNECT_MESSAGE)){ // process nodes infomation
        	   // get response
               String received = inFromServer.readLine();
               LOGGER.info("Server said: " + received);
           }else if(content.equals(SystemConstants.FILE_NAME_MESSAGE)){ // get list file sharing
        	   boolean keepGoing = true;
        	   ArrayList<String> lstFileName = new ArrayList<String>();
        	   while (keepGoing){
        		   try{      	
        			   // get response
        			   String received = inFromServer.readLine();
        			   if(received.equals(SystemConstants.FLAG_MESSAGE)){
        				   break;
        			   }
        			   lstFileName.add(received);
        			   LOGGER.info("Server said: " + received);
		          	}catch (SocketTimeoutException ste){
		          		keepGoing = false;
		          	}
        	   }
        	   ListFileName.getInstance().setFileNames(lstFileName);
           }else{
			   
           }
           socket.close();
           return true;
	      }
		  catch (SocketTimeoutException ste)
	      {
			  try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	      } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  return false;
	}
}
