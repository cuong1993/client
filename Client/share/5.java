package threads;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;
import java.util.logging.Logger;

import constants.SystemConstants;
import event.EventInfo;
import event.EventAction;

import utils.Folder;

public class UploadFileThread extends Thread{
	private static Logger LOGGER = Logger.getLogger(UploadFileThread.class.getName());
	private String posfile = null;
	private transient Vector listeners;
	public UploadFileThread(String posfile) {
		super();
		this.posfile = posfile;
	}
	private void Upload(){
		File file = new File(posfile);
		String filename = file.getName();
		long filesize = file.length();
		String dir = file.getAbsolutePath();
		long chunksize = SystemConstants.CHUNK_SIZE * 1024;
		int count = (int) (filesize / chunksize);
		if(count * chunksize < filesize){
			count++;
		}
        int numberChunk = count;
        String direcChunk = SystemConstants.CHUNK_FOLDER + "/" + filename + "/";
        Folder folder = new Folder();
        folder.createFolder(direcChunk);
        FileInputStream fis = null;
        try {
	        if (listeners != null && !listeners.isEmpty()){
				Vector targets;
	            synchronized (this) {
	                targets = (Vector) listeners.clone();
	            }
	            EventInfo event = new EventInfo();
	            event.setMaxValue(numberChunk);
	         // Start event
				Enumeration e = targets.elements();
	            while (e.hasMoreElements()) {
	                EventAction l = (EventAction) e.nextElement();
	                l.onStart(event);
	            }
	            fis = new FileInputStream(dir);
		        for(int i = 0; i < numberChunk; i++){
		        	byte[] buffer = new byte[(int) chunksize];
		        	int readsize = fis.read(buffer);
		        	byte[] newbuffer = new byte[readsize];
		        	
		        	System.arraycopy(buffer, 0, newbuffer, 0, readsize);
		        	createChunk(i + 1, newbuffer, direcChunk, filename);
		        	
		        	event.setValue(i + 1);
					// Event Occur
					e = targets.elements();
                    while (e.hasMoreElements()) {
                        EventAction l = (EventAction) e.nextElement();
                        l.onOccur(event);
                    }
		        }
		        //Event Finish
                e = targets.elements();
                while (e.hasMoreElements()) {
                    EventAction l = (EventAction) e.nextElement();
                    l.onFinish(event);
                }
			}
	        LOGGER.info("Upload file finish!");
        } catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void run(){
		Upload();
	}
	/**
	 * Create file .chunk
	 */
	public boolean createChunk(int idChunk, byte[] data, String dir, String filename){
		FileOutputStream out = null;
		File fileChunk = new File(dir + filename + SystemConstants.CHARACTERS_FILE + idChunk + SystemConstants.EXTENSION_FILE);
		try {
			out = new FileOutputStream(fileChunk);
			out.write(data);
	        out.flush();
	        out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	/**
    * register event
    */
   synchronized public void addCustomListener(EventAction l) {
       if (listeners == null) {
           listeners = new Vector();
       }
       listeners.addElement(l);
   }
   /**
    * delete event
    */
   synchronized public void removeCustomListener(EventAction l) {
       if (listeners == null) {
           listeners = new Vector();
       }
       listeners.removeElement(l);
   }
}
