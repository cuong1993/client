package threads;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

import constants.SystemConstants;

import dto.ComputerInfo;

public class ListeningThread extends Thread{
	private static Logger LOGGER = Logger.getLogger(ListeningThread.class.getName());
	private ServerSocket socket = null;
    private boolean moreQuotes = true;
	/**
	 * listen all clients request to this computer
	 */
	private void listen() {
		try {
			socket = new ServerSocket(ComputerInfo.getInstance().getPortNumber());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOGGER.info("This is port " + ComputerInfo.getInstance().getPortNumber() + " is used");
			moreQuotes = false;
		}
        while (moreQuotes) {
            try {
                Socket connectionSocket = socket.accept();
                // receive request
                BufferedReader inFromClient =
      	              new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                String received = inFromClient.readLine();
                LOGGER.info("Client said: " + received);
                
                // figure out response
                if(received.equals(SystemConstants.CONNECT_MESSAGE)){ // send respond to client
                	DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
                    outToClient.writeBytes(SystemConstants.LISTEN_MESSAGE + '\n');
                }else if(received.equals(SystemConstants.FILE_NAME_MESSAGE)){ // send list file name is sharing to client
                	File folder = new File(SystemConstants.CHUNK_FOLDER);
    				String []results = folder.list();
    		        if(null != results){
    		        	for (int i = 0; i < results.length; i++) {
    		        		DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
        		        	outToClient.writeBytes(results[i] + '\n');
        		        }
    		        }
    		        DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
    		        outToClient.writeBytes(SystemConstants.FLAG_MESSAGE + '\n');
                }else{ // send file .chunk when client request download
                	String dir = SystemConstants.CHUNK_FOLDER + "/" +received + "/"; 
                	File folder = new File(dir);
                    File[] listOfFiles = folder.listFiles();
                    
                    String numberChunk = listOfFiles.length + "";
                    OutputStream os = connectionSocket.getOutputStream();
                    DataOutputStream outToClient = new DataOutputStream(os);
    		        outToClient.writeBytes(numberChunk + '\n');
                    for (int i = 0; i < listOfFiles.length; i++) {
                    	int id = i + 1;
                    	String dirDownLoad = dir + received + SystemConstants.CHARACTERS_FILE + id + 
                    			SystemConstants.EXTENSION_FILE;
                    	File file = new File(dirDownLoad);
                    	if(i == listOfFiles.length - 1){
                    		outToClient = new DataOutputStream(os);
            		        outToClient.writeBytes(file.length() + "" + '\n');
                    	}
                    	byte[] buf = new byte[(int)file.length()];
                    	FileInputStream fis = new FileInputStream(file);
                    	BufferedInputStream bis = new BufferedInputStream(fis);
                    	bis.read(buf,0,buf.length);
                    	os.write(buf,0,buf.length);
                        os.flush();
                        if (bis != null) bis.close();
                    }
                    os.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                moreQuotes = false;
            }
        }
        if(null != socket){
        	try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }       
	}
	
	public void run(){
		listen();
	}
}
